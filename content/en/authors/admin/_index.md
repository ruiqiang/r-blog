---
bio: 
  matter.
education:
  courses:
  - course: Master of Statistics
    institution: SouthWest JiaoTong University
    year: 2017
email: ""
highlight_name: false
interests:
- Artificial Intelligence
- Information Retrieval
social:
- icon: twitter
  icon_pack: fab
  label: Follow me on Twitter
  link: https://twitter.com/ruiqiang
- icon: github
  icon_pack: fab
  link: https://github.com/ruiqiangJiao
- icon: linkedin
  icon_pack: fab
  link: http://www.linkedin.com/in/%E7%91%9E%E5%BC%BA-%E7%84%A6-874a04144/s
- icon: weibo
  icon_pack: fab
  link: https://weibo.com/ruiqiangjiao?is_all=1
- icon: google-plus
  icon_pack: fab
  link: https://plus.google.com/u/0/113298146551198923726
superuser: true
title: Ruiqiang
---

Ruiqiang Jiao is a Data Analyst who has a great passion for Data Science as well as Economics and Finance. He is now on his way to being a Professional Financial Analyst and Data Scientist.
