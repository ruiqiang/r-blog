---
education:
  courses:
  - course: 统计学研究生
    institution: 西南交通大学
    year: 2017
email: ""
highlight_name: false
interests:
- 人工智能
- 信息检索
role: 
social:
- icon: twitter
  icon_pack: fab
  link: https://twitter.com/ruiqiang
- icon: github
  icon_pack: fab
  link: https://github.com/ruiqiangJiao
- icon: linkedin
  icon_pack: fab
  link: http://www.linkedin.com/in/%E7%91%9E%E5%BC%BA-%E7%84%A6-874a04144/s
- icon: weibo
  icon_pack: fab
  link: https://weibo.com/ruiqiangjiao?is_all=1
- icon: google-plus
  icon_pack: fab
  link: https://plus.google.com/u/0/113298146551198923726
---


