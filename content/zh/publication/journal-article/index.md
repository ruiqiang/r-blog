---
abstract: 文章基于贝叶斯学习，将正则化方法从贝叶斯分析的角度展开，在响应变量服从正态分布、回归系数服从指数型先验分布族的条件下，用贝叶斯准则给出了惩罚因子的取值与响应变量、系数的方差之间的关系，并将这一结果应用到岭回归和 lasso 回归中惩罚因子的选择。最后用实际的数据进行检验，结果表明，当响应变量和系数服从正态分布，惩罚因子的值取二者方差商的方法比岭迹法和广义交叉验证法 (GCV) 拟合效果更优。
author_notes:
- Equal contribution
- Equal contribution
- Equal contribution
- Equal contribution
authors:
- 焦瑞强
- 赵联文
- 刘赪
- 任桃红
date: "2017-08-28"
doi: "10.13546/j.cnki.tjyjc.2017.14.002"
projects: []
publication: 《统计与决策》
publication_short: ""
publication_types:
- "2"
publishDate: "2017-08-28"
slides: 基于贝叶斯学习的惩罚因子的选择
summary: 
tags:
- 贝叶斯
title: 基于贝叶斯学习的惩罚因子的选择
url_code: ""
url_dataset: ""
url_pdf: http://www.cnki.net/kcms/doi/10.13546/j.cnki.tjyjc.2017.14.002.html
url_poster: ""
url_project: ""
url_slides: "https://ruiqiang.netlify.com/slide/paper_slide.html"
url_source: ""
url_video: ""
---

